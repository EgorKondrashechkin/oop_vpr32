module com.example.laba1tast1 {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires java.desktop;
    requires javafx.swing;

    opens com.example.laba1tast1 to javafx.fxml;
    exports com.example.laba1tast1;
}
package com.example.laba1tast1;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javax.imageio.ImageIO;
import java.io.File;
import javafx.embed.swing.SwingFXUtils;

public class HelloController {
    @FXML
    private Pane draw_pane;

    @FXML
    private ImageView picture_1;

    @FXML
    private ImageView selectedPicture;

    @FXML
    private ImageView picture_2;

    @FXML
    private ImageView picture_3;

    @FXML
    private Button save_buttton;

    @FXML
    private ToggleGroup selectPicturces;

    @FXML
    private ToggleGroup selectPicturces2;

    @FXML
    private ToggleGroup selectPicturces3;

    @FXML
    private ToggleButton tonggle_button_1;

    @FXML
    private ToggleButton tonggle_button_2;

    @FXML
    private ToggleButton tonggle_button_3;

    private FileChooser fileChooser = new FileChooser();

    public void selectTonggleButton(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();
        if (source == tonggle_button_1) {
            System.out.println("1");
            selectedPicture = picture_1;
        } else if (source == tonggle_button_2) {
            System.out.println("2");
            selectedPicture = picture_2;
        } else if (source == tonggle_button_3) {
            System.out.println("3");
            selectedPicture = picture_3;
        }
    }

    public void drawPicture(MouseEvent mouseEvent) {
        ImageView draw_picture = new ImageView(selectedPicture.getImage());

        draw_picture.setLayoutX(mouseEvent.getX());
        draw_picture.setLayoutY(mouseEvent.getY());

        draw_pane.getChildren().add(draw_picture);
    }

    public void sreenDrawPanel(ActionEvent actionEvent) {
        Window stage = draw_pane.getScene().getWindow();
        fileChooser.setTitle("Save your picture");
        fileChooser.setInitialFileName("mypicture");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG file", "*.png"),
                new FileChooser.ExtensionFilter("JPG file", "*.jpg")
        );

        try {
            File file = fileChooser.showSaveDialog(stage);
            fileChooser.setInitialDirectory(file.getParentFile());

            File f = new File(file.getAbsolutePath());

            String fileExtension = f.getName().substring(f.getName().lastIndexOf(".") + 1);

            if(f != null) {
                WritableImage wi = new WritableImage(500,500);
                draw_pane.snapshot(null, wi);
                ImageIO.write(SwingFXUtils.fromFXImage(wi,null),fileExtension,f);

            }


        } catch (Exception e) {
            System.out.println("lol");
        }
    }
}
package com.example.laba1task4;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.shape.Line;

import java.util.ArrayList;

public class MainController {

    @FXML
    private TextField argument_step;

    @FXML
    private ComboBox<String> box_function;

    @FXML
    private LineChart<?, ?> chart;

    @FXML
    private TextField max_value;

    @FXML
    private TextField min_value;

    @FXML
    private ToggleGroup toggle_group;

    @FXML
    private TextField width_line;

    @FXML
    private RadioButton hide;

    @FXML
    private RadioButton show;

    private ArrayList<String> allField = new ArrayList<>();

    private Function func1 = new Function();

    private Function func2 = new Function();

    private Function func3 = new Function();

    @FXML
    void initialize() {
        ObservableList<String> list = FXCollections.observableArrayList(
                "y(x)=sin(x)",
                "y(x)=cos(x)-2*sin(x)",
                "y(x)=sin(x) * sin (x)");
        box_function.setItems(list);
        chart.setCreateSymbols(false);

    }

    public void drawFunction() {
       if (!fieldsNotEmpty()) {
         System.out.println("Заполнены не все поля");
       } else {
           double min = Double.parseDouble(min_value.getText());
           double max = Double.parseDouble(max_value.getText());
           double step = Double.parseDouble(argument_step.getText());
           try {
               switch (box_function.getValue()) {
                   case ("y(x)=sin(x)"): {
                       func1.width = Double.parseDouble(width_line.getText());
                       func1.step = Double.parseDouble(argument_step.getText());
                       func1.min = Double.parseDouble(min_value.getText());
                       func1.max = Double.parseDouble(max_value.getText());
                       func1.view = show.isSelected();

                       if (func1.series != null) {
                           chart.getData().removeAll(func1.series);
                           func1.series = new XYChart.Series();
                       } else {
                           func1.series = new XYChart.Series();
                       }

                       double y = 0;

                       if (!stepIsNull(step)) {
                           for (double i = min; i <= max; i += step) {
                               double x = i;
                               y = Math.sin(i);
                               func1.series.getData().add(
                                       new XYChart.Data<>(String.valueOf(x), y)
                               );
                           }

                           func1.series.setName("y(x)=sin(x)");
                           chart.getData().add(func1.series);
                           func1.series.getNode()
                                   .lookup(".chart-series-line")
                                   .setStyle("-fx-stroke-width: " + width_line.getText() + " ;");
                           func1.series.getNode().setVisible(func1.view);
                           func1.series.getNode().lookup(".chart-line-symbol").setStyle("-fx-background-color: #f9d900, white; -fx-background-insets: 0, 2; -fx-background-radius: 5px; -fx-padding: 5px;");
                       } else {
                           System.out.println("Шаг не может быть 0");
                       }
                       break;
                   }
                   case ("y(x)=cos(x)-2*sin(x)"): {
                       func2.width = Double.parseDouble(width_line.getText());
                       func2.step = Double.parseDouble(argument_step.getText());
                       func2.min = Double.parseDouble(min_value.getText());
                       func2.max = Double.parseDouble(max_value.getText());
                       func2.view = show.isSelected();

                       if (func2.series != null) {
                           chart.getData().removeAll(func2.series);
                           func2.series = new XYChart.Series();
                       } else {
                           func2.series = new XYChart.Series();
                       }

                       double y = 0;
                       if (!stepIsNull(step)) {
                           for (double i = min; i <= max; i += step) {
                               double x = i;
                               y = Math.cos(i) - 2 * Math.sin(i);
                               func2.series.getData().add(
                                       new XYChart.Data<>(String.valueOf(x), y)
                               );
                           }
                           func2.series.setName("y(x)=cos(x)-2*sin(x)");
                           chart.getData().add(func2.series);
                           func2.series.getNode()
                                   .lookup(".chart-series-line")
                                   .setStyle("-fx-stroke-width: " + width_line.getText() + " ;");
                           func2.series.getNode().setVisible(func2.view);
                       } else {
                           System.out.println("Шаг не может быть 0");
                       }
                       break;
                   }
                   case ("y(x)=sin(x) * sin (x)"): {
                       func3.width = Double.parseDouble(width_line.getText());
                       func3.step = Double.parseDouble(argument_step.getText());
                       func3.min = Double.parseDouble(min_value.getText());
                       func3.max = Double.parseDouble(max_value.getText());
                       func3.view = show.isSelected();

                       if (func3.series != null) {
                           chart.getData().removeAll(func3.series);
                           func3.series = new XYChart.Series();
                       } else {
                           func3.series = new XYChart.Series();
                       }

                       double y = 0;
                       if (!stepIsNull(step)) {
                           for (double i = min; i <= max; i += step) {
                               double x = i;
                               y = Math.sin(i) * Math.sin(i);
                               func3.series.getData().add(
                                       new XYChart.Data<>(String.valueOf(x), y)
                               );
                           }
                           func3.series.setName("y(x)=sin(x) * sin (x)");
                           chart.getData().add(func3.series);
                           func3.series.getNode()
                                   .lookup(".chart-series-line")
                                   .setStyle("-fx-stroke-width: " + width_line.getText() + " ;");
                           func3.series.getNode().setVisible(func3.view);
                       } else {
                           System.out.println("Шаг не может быть 0");
                       }
                       break;
                   }
               }
           } catch (Exception e) {
               System.out.println("Вы не выбрали функцию");
           }
       }
    }

    public boolean fieldsNotEmpty() {
        allField.add(min_value.getText());
        allField.add(max_value.getText());
        allField.add(width_line.getText());
        allField.add(argument_step.getText());

        for (var field : allField) {
            if (field.isEmpty()){
                allField.clear();
                return false;
            }
        }
        allField.clear();
        return true;
    }

    public void setFunctionParameter() {
        try {
            switch (box_function.getValue()) {
                case ("y(x)=sin(x)"): {
                    getValueFunction(func1);
                    break;
                }
                case ("y(x)=cos(x)-2*sin(x)"): {
                    getValueFunction(func2);
                    break;
                }
                case ("y(x)=sin(x) * sin (x)"): {
                    getValueFunction(func3);
                    break;
                }
            }
        } catch (Exception e) {}
    }


    private void getValueFunction(Function func) {
        width_line.setText(String.valueOf(func.width));
        argument_step.setText(String.valueOf(func.step));
        min_value.setText(String.valueOf(func.min));
        max_value.setText(String.valueOf(func.max));

        if (func.view) {
            show.setSelected(true);
        } else {
            hide.setSelected(true);
        }
    }
    private boolean stepIsNull(double step) {
        return step == 0;
    }
}
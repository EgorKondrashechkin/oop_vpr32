package com.example.laba1task4;

interface FunctionInterface {
    abstract double getValueFunction(double min, double max, double step);
}

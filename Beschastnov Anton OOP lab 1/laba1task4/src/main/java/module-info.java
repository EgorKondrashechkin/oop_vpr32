module com.example.laba1task4 {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;

    opens com.example.laba1task4 to javafx.fxml;
    exports com.example.laba1task4;
}
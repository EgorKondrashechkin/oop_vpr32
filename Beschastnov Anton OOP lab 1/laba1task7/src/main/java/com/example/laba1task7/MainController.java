package com.example.laba1task7;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Duration;

public class MainController {

    @FXML
    private AnchorPane AnchorPaneBox;

    @FXML
    private Button buttonPPR;

    @FXML
    private HBox hBoxContoller;

    @FXML
    private Label hBoxVolume;

    @FXML
    private Label labelCurrentTime;

    @FXML
    private Label labelTotalTime;

    @FXML
    private MediaView mvVideo;
    private MediaPlayer mpVideo;
    private Media mediaVideo;

    @FXML
    private Slider sliderTime;

    @FXML
    private Slider sliderVolume;

    @FXML
    private Label volumValue;

    private static File file;
    private boolean atEndOfVideo = false;
    private boolean isPlaying = true;

    @FXML
    void initialize() {
        file = new File("123.mp4");
        mediaVideo = new Media(file.toURI().toString());
        mpVideo = new MediaPlayer(mediaVideo);
        mvVideo.setMediaPlayer(mpVideo);
        volumValue.setText("0");

        buttonPPR.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Button buttonPlay = (Button) actionEvent.getSource();
//                if (atEndOfVideo) {
//                    sliderTime.setValue(0);
//                    atEndOfVideo = false;
//                    isPlaying = false;
//                }

                if (isPlaying) {
                    buttonPlay.setText("Play");
                    mpVideo.pause();
                    isPlaying = false;
                } else {
                    buttonPlay.setText("Stop");
                    mpVideo.play();
                    isPlaying = true;
                }
            }
        });

        mpVideo.volumeProperty().bindBidirectional(sliderVolume.valueProperty());

        bindCurrentTimeLabel();

        sliderVolume.valueProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                mpVideo.setVolume((sliderVolume.getValue()));
                volumValue.setText(String.valueOf(mpVideo.volumeProperty().getValue()));
            }
        });

        mpVideo.totalDurationProperty().addListener(new ChangeListener<Duration>() {
            @Override
            public void changed(ObservableValue<? extends Duration> observableValue, Duration oldDuration, Duration newDuration) {
                sliderTime.setMax(newDuration.toSeconds());
                labelTotalTime.setText(getTime(newDuration));
            }
        });

        sliderTime.valueChangingProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean wasChanging, Boolean isChanging) {
                if (!isChanging) {
                    mpVideo.seek(Duration.seconds(sliderTime.getValue()));
                }
            }
        });

        sliderTime.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
                double currentTime = mpVideo.getCurrentTime().toSeconds();
                if ((Math.abs(currentTime - newValue.doubleValue())) > 0.5) {
                    mpVideo.seek(Duration.seconds(newValue.doubleValue()));
                }

                labelMatchEndVideo(labelCurrentTime.getText(), labelTotalTime.getText());
            }
        });

        mpVideo.currentTimeProperty().addListener(new ChangeListener<Duration>() {
            @Override
            public void changed(ObservableValue<? extends Duration> observableValue, Duration oldTime, Duration newTime) {
                if (!sliderTime.isValueChanging()) {
                    sliderTime.setValue(newTime.toSeconds());
                }
                labelMatchEndVideo(labelCurrentTime.getText(), labelTotalTime.getText());

            }
        });

//        mpVideo.setOnEndOfMedia(new Runnable() {
//            @Override
//            public void run() {
//                buttonPPR.setText("repit");
//                atEndOfVideo = true;
//                if (!labelCurrentTime.textProperty().equals(labelTotalTime.textProperty())) {
//                    labelCurrentTime.textProperty().unbind();
//                    labelCurrentTime.setText(getTime(mpVideo.getTotalDuration()) + " / ");
//                }
//            }
//        });
    }

    public static String getNameFile() {
        return file.getName();
    }

    public void bindCurrentTimeLabel() {
        labelCurrentTime.textProperty().bind(
                Bindings.createStringBinding(
                        new Callable<String>() {
                            @Override
                            public String call() throws Exception {
                                return getTime(mpVideo.getCurrentTime()) + " / ";
                            }
                        } , mpVideo.currentTimeProperty()
                )
        );
    }

    public String getTime(Duration time){

        int hours = (int) time.toHours();
        int min = (int) time.toMinutes();
        int sec =(int) time.toSeconds();

        if (sec > 59) sec = sec%60;
        if (min > 59) min = min%60;
        if (hours > 59) hours = hours%60;

        if (hours > 0) {
            return String.format("%d:%02d:%02d", hours, min, sec);
        } else {
            return String.format("%02d:%02d", min, sec);
        }
    }

    public void labelMatchEndVideo(String labelTime, String labelTotalTime) {
        for (int i = 0; i < labelTotalTime.length(); i++) {
            if (labelTime.charAt(i) != labelTotalTime.charAt(i)) {
                atEndOfVideo = false;
                if (isPlaying) buttonPPR.setText("stop");
                else buttonPPR.setText("play");
                break;
            }
//            else {
//                atEndOfVideo = true;
//                buttonPPR.setText("Repit");
//            }
        }
    }
}
module com.example.laba1task7 {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.swing;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires javafx.media;

    opens com.example.laba1task7 to javafx.fxml;
    exports com.example.laba1task7;
}
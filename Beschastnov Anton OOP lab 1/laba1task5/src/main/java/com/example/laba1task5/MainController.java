package com.example.laba1task5;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;

public class MainController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private CheckBox check_box_1;

    @FXML
    private CheckBox check_box_2;

    @FXML
    private CheckBox check_box_3;

    @FXML
    private Button add_button;

    @FXML
    private TextField author_field;

    @FXML
    private TextField language_field;

    @FXML
    private TextField year_field;

    @FXML
    private TableColumn<Person, String> author;

    @FXML
    private TableColumn<Person, String> language;

    @FXML
    private TableColumn<Person, String> year;

    @FXML
    private TableView<Person> table;

    final ObservableList<Person> data = FXCollections.observableArrayList(
            new Person("Си", "Деннис Ритчи", "1972"),
            new Person("C++", " Бьерн Cтрауструп", "1983"),
            new Person("Python", "Гвидо ван Россум", "1991"),
            new Person("Java", "Джеймс Гослинг", "1995"),
            new Person("JavaScript", "Брендон Айк", "1995"),
            new Person(" C#", "Андерс Хейлсберг", "2001"),
            new Person(" Scala#", "Мартин Одерски", "2003")
    );

    @FXML
    void initialize() {
        language.setCellValueFactory(
                new PropertyValueFactory<>("language")
        );

        language.setCellFactory(TextFieldTableCell.forTableColumn());
        language.setOnEditCommit(
                (TableColumn.CellEditEvent<Person, String> t) -> {
                    (t.getTableView().getItems().get(
                            t.getTablePosition().getRow())
                    ).setLanguage(t.getNewValue());
                });
        author.setCellFactory(TextFieldTableCell.forTableColumn());
        author.setOnEditCommit(
                (TableColumn.CellEditEvent<Person, String> t) -> {
                    (t.getTableView().getItems().get(
                            t.getTablePosition().getRow())
                    ).setAuthor(t.getNewValue());
                });

        year.setCellFactory(TextFieldTableCell.forTableColumn());

        year.setOnEditCommit(
                (TableColumn.CellEditEvent<Person, String> t) -> {
                    (t.getTableView().getItems().get(
                            t.getTablePosition().getRow())
                    ).setYear((t.getNewValue()));
                });

        author.setCellValueFactory(
                new PropertyValueFactory<>("author")
        );
        year.setCellValueFactory(
                new PropertyValueFactory<Person, String>("year")
        );

        table.setEditable(true);
        table.setItems(data);
    }

    public void addAnEntry(ActionEvent actionEvent) {
        data.add(new Person(
                language_field.getText(),
                author_field.getText(),
                year_field.getText()
        ));
    }

    public void setVisibleColumn(ActionEvent actionEvent) {
            language.setVisible(!check_box_1.isSelected());
            author.setVisible(!check_box_2.isSelected());
            year.setVisible(!check_box_3.isSelected());
    }
}

package com.example.laba1task5;

public class Person {
    private String year;
    private String language;
    private String author;

    public Person(String language, String author, String year) {
        this.language = language;
        this.author = author;
        this.year = year;
    }

    public Person() {
    }

    public String getYear() {
        return year;
    }

    public String getLanguage() {
        return language;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}

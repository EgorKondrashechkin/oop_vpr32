package com.example.laba1task2;

import javafx.application.Application;
import javafx.css.CssMetaData;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class Main extends Application {

    Shape[] shapes = {
            new Line(0,0,125,125),
            new Rectangle(0,0,125,125),
            new Circle(0,0,100),
            new Ellipse(0,0,100,50)
    };

    @Override
    public void start(Stage stage) throws IOException {
        Group root = new Group();
        VBox ramka = new VBox();
        System.out.println(shapes.length - 1);
        Shape myshape = shapes[rnd(shapes.length - 1)];
        myshape.setFill(Paint.valueOf("#1e90ff"));
        ramka.setStyle("-fx-border-style: dashed;-fx-border-width: 5;-fx-border-color: #00000080;");
        ramka.getChildren().add(myshape);
        Scene scene = new Scene(root, 800, 600);
        stage.setTitle("Task #2");
        stage.setScene(scene);
        root.getChildren().add(ramka);
        stage.show();

        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                switch (keyEvent.getCode()) {
                    case  W:
                        ramka.setLayoutY(ramka.getLayoutY() - 10);
                        break;
                    case S:
                        ramka.setLayoutY(ramka.getLayoutY() + 10);
                        break;
                    case A:
                        ramka.setLayoutX(ramka.getLayoutX() - 10);
                        break;
                    case RIGHT:
                        ramka.setLayoutX(ramka.getLayoutX() + 10);
                        break;
                    case COMMA:
                        setXSize(myshape, -1);
                        break;
                    case PERIOD:
                        setXSize(myshape, 1);
                        break;
                    case MINUS:
                        setYSize(myshape, -1);
                        break;
                    case EQUALS:
                        setYSize(myshape, 1);
                        break;
                }
            }
        });
    }

    public void setYSize(Shape sp, int step) {
        if (Circle.class.equals(sp.getClass())) {
            ((Circle) sp).setRadius(((Circle) sp).getRadius() + step);
        } else if (Rectangle.class.equals(sp.getClass())) {
            ((Rectangle) sp).setHeight(((Rectangle) sp).getHeight() + step);
        } else if (Ellipse.class.equals(sp.getClass())) {
            ((Ellipse) sp).setRadiusY(((Ellipse) sp).getRadiusY() + step);
        } else if (Line.class.equals(sp.getClass())) {
            ((Line) sp).setEndY(((Line) sp).getEndY() + step);
        }
    }

    public void setXSize(Shape sp, int step) {
        if (Circle.class.equals(sp.getClass())) {
            ((Circle) sp).setRadius(((Circle) sp).getRadius() + step);
        } else if (Rectangle.class.equals(sp.getClass())) {
            ((Rectangle) sp).setWidth(((Rectangle) sp).getWidth() + step);
        } else if (Ellipse.class.equals(sp.getClass())) {
            ((Ellipse) sp).setRadiusX(((Ellipse) sp).getRadiusX() + step);
        } else if (Line.class.equals(sp.getClass())) {
            ((Line) sp).setEndX(((Line) sp).getEndX() + step);
        }
    }

    public static int rnd(final double max)
    {
        return (int) (Math.random() * max);
    }

    public static void main(String[] args) {
        launch();
    }
}
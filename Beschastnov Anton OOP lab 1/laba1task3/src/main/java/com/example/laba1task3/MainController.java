package com.example.laba1task3;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.*;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MainController {

    @FXML
    private ToggleButton circle_button;

    @FXML
    private ColorPicker conture_color;

    @FXML
    private AnchorPane draw_panel;

    @FXML
    private ToggleButton ellipse_button;

    @FXML
    private ColorPicker fill_color;

    @FXML
    private TextField hingth_picture;

    @FXML
    private ToggleButton line_button;

    @FXML
    private MenuBar menu;

    @FXML
    private MenuItem menu_exit;

    @FXML
    private MenuItem menu_helper;

    @FXML
    private MenuItem menu_save;

    @FXML
    private ToggleButton ractangle_button;

    @FXML
    private ToggleGroup shape_toggle;

    @FXML
    private ComboBox<String> type_conture;

    @FXML
    private TextField width_conture;

    @FXML
    private TextField width_picture;

    private static VBox selectedShape;

    private FileChooser fileChooser = new FileChooser();


    @FXML
    void initialize() {
        ObservableList<String> list = FXCollections.observableArrayList("dotted", "pointed" , "solid");
        type_conture.setItems(list);

    }

    public Shape getSelectButton(){
            if (ractangle_button.isSelected()){
                return new Rectangle(0,0,100,100);}
            if (ellipse_button.isSelected()){
                return new Ellipse(0,0,100,50);}
            if (circle_button.isSelected()){
                return new Circle(0,0,100);}
            if (line_button.isSelected()){
                return new Line(0,0,100,50);}

        return  null;
    }


    public void drawShape(MouseEvent mouseEvent) {
        if (selectedShape == null) {
            selectedShape = new VBox();
        } else {
            selectedShape.setStyle("-fx-border-style: dashed;-fx-border-width: 0;-fx-border-color: #00000080;");
            selectedShape = new VBox();
        }

        Shape shape = getSelectButton();
        shape.setFill(fill_color.getValue());
        shape.setStroke(Paint.valueOf(String.valueOf(conture_color.getValue())));

        double width;

        try {
             width = Double.parseDouble(width_conture.getText());
        } catch (Exception e) {
             width = 1;
        }
        shape.setStrokeWidth(width);
        shape.setStrokeLineCap(StrokeLineCap.BUTT);type_conture.getValue();

        try {
            switch (type_conture.getValue()) {
                case "dotted":
                    shape.setStyle("-fx-stroke-dash-array: 11;");
                    shape.setStrokeLineCap(StrokeLineCap.BUTT);
                    break;
                case "pointed":
                    shape.setStyle("-fx-stroke-dash-array: 2;");
                    shape.setStrokeLineCap(StrokeLineCap.BUTT);
                    break;
            }
        } catch (Exception e) {}


        selectedShape.setStyle("-fx-border-style: dashed;-fx-border-width: 3;-fx-border-color: #00000080;");
        selectedShape.getChildren().add(shape);

        selectedShape.setLayoutY(mouseEvent.getY());
        selectedShape.setLayoutX(mouseEvent.getX());

        draw_panel.getChildren().add(selectedShape);
    }

    public static void managementShape(KeyEvent event) {
        switch (event.getCode()) {
            case  W:
                selectedShape.setLayoutY(selectedShape.getLayoutY() - 10);
                break;
            case S:
                selectedShape.setLayoutY(selectedShape.getLayoutY() + 10);
                break;
            case A:
                selectedShape.setLayoutX(selectedShape.getLayoutX() - 10);
                break;
            case D:
                selectedShape.setLayoutX(selectedShape.getLayoutX() + 10);
                break;
            case COMMA:
                setXSize((Shape) selectedShape.getChildren().get(0), -1);
                break;
            case PERIOD:
                setXSize((Shape) selectedShape.getChildren().get(0), 1);
                break;
            case MINUS:
                setYSize((Shape) selectedShape.getChildren().get(0), -1);
                break;
            case EQUALS:
                setYSize((Shape) selectedShape.getChildren().get(0), 1);
                break;
        }
    }

    public static void setYSize(Shape sp, int step) {
        System.out.println( sp.getClass().getName());
        if (Circle.class.equals(sp.getClass())) {
            ((Circle) sp).setRadius(((Circle) sp).getRadius() + step);
        } else if (Rectangle.class.equals(sp.getClass())) {
            ((Rectangle) sp).setHeight(((Rectangle) sp).getHeight() + step);
        } else if (Ellipse.class.equals(sp.getClass())) {
            ((Ellipse) sp).setRadiusY(((Ellipse) sp).getRadiusY() + step);
        } else if (Line.class.equals(sp.getClass())) {
            ((Line) sp).setEndY(((Line) sp).getEndY() + step);
        }
    }

    public static void setXSize(Shape sp, int step) {
        if (Circle.class.equals(sp.getClass())) {
            ((Circle) sp).setRadius(((Circle) sp).getRadius() + step);
        } else if (Rectangle.class.equals(sp.getClass())) {
            ((Rectangle) sp).setWidth(((Rectangle) sp).getWidth() + step);
        } else if (Ellipse.class.equals(sp.getClass())) {
            ((Ellipse) sp).setRadiusX(((Ellipse) sp).getRadiusX() + step);
        } else if (Line.class.equals(sp.getClass())) {
            ((Line) sp).setEndX(((Line) sp).getEndX() + step);
        }
    }

    public void sreenDrawPanel(ActionEvent actionEvent) {
        Window stage = draw_panel.getScene().getWindow();
        fileChooser.setTitle("Save your picture");
        fileChooser.setInitialFileName("mypicture");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG file", "*.png"),
                new FileChooser.ExtensionFilter("JPG file", "*.jpg")
        );

        try {
            File file = fileChooser.showSaveDialog(stage);
            fileChooser.setInitialDirectory(file.getParentFile());

            File f = new File(file.getAbsolutePath());

            String fileExtension = f.getName().substring(f.getName().lastIndexOf(".") + 1);

            if(f != null) {
                Image image = draw_panel.snapshot(null, null);

                BufferedImage bufferedImageInput = SwingFXUtils.fromFXImage(image, null);

                BufferedImage bufferedImageOutput = new BufferedImage(
                        Integer.parseInt(width_picture.getText()),
                        Integer.parseInt(hingth_picture.getText()),
                        bufferedImageInput.getType()
                );

                Graphics2D g2d = bufferedImageOutput.createGraphics();
                g2d.drawImage(
                        bufferedImageInput,
                        0,
                        0,
                        Integer.parseInt(width_picture.getText()),
                        Integer.parseInt(hingth_picture.getText()),
                        null
                );
                g2d.dispose();

                ImageIO.write(bufferedImageOutput, fileExtension, f);
            }
        } catch (Exception e) {
            System.out.println("lol");
        }
    }

    public void handleCloseButtonAction() {
        Stage stage = (Stage) draw_panel.getScene().getWindow();
        stage.close();
    }

    public void openHelper(ActionEvent actionEvent) {
    }
}
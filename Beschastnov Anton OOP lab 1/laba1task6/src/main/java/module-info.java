module com.example.laba1task6 {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.swing;
    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;

    opens com.example.laba1task6 to javafx.fxml;
    exports com.example.laba1task6;
}
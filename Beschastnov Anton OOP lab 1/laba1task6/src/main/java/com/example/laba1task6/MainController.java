package com.example.laba1task6;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point3D;
import javafx.scene.Camera;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.Sphere;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Transform;
import javafx.scene.transform.Translate;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import javax.imageio.ImageIO;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;


public class MainController {

    @FXML
    private Pane draw_panel;

    @FXML
    private MenuItem save_button;

    @FXML
    private Button select_button;

    private FileChooser fileChooser = new FileChooser();

    private File fileShape;

    static SmartGroup molecule = new SmartGroup();

    HashMap<String, Integer> map = new HashMap<String, Integer>();

    public void getFile(ActionEvent actionEvent) {
        Window stage = draw_panel.getScene().getWindow();

        fileChooser.setTitle("Load yor file");

        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("3DShape file","*.xyz"));

        try {
            File fileShape = fileChooser.showOpenDialog(stage);
            fileChooser.setInitialDirectory(fileShape.getParentFile());

            BufferedReader br = new BufferedReader(new FileReader(fileShape));
            Stream<String> info = br.lines();
            List<String> listInfo = info.toList();
            drawMolekul(listInfo);

            br.close();

        }catch (Exception e) {
            System.out.println("Какая-то ошибка!");
        }
    }

    private void drawMolekul(List<String> listInfo) {

        molecule.getChildren().clear();

        Molekule molekuleShape = new Molekule();

        Camera cam = new PerspectiveCamera();

        draw_panel.getScene().setCamera(cam);

        molekuleShape.setDescription(listInfo.get(1));

        for (int i = 2; i <= listInfo.size() - 1; i++) {
            molekuleShape.getAllAtom().add(new Atom(listInfo.get(i)));
        }

        for (var atom : molekuleShape.getAllAtom()) {
            StackPane stack = new StackPane();

            atom.setAPositionStack(stack);
            atom.setAtomParametr();

            molecule.getChildren().add(stack);
        }

        molekuleShape.setConnection(molecule);

        molecule.translateXProperty().set(draw_panel.getWidth()/2);
        molecule.translateYProperty().set(draw_panel.getHeight()/2);

        draw_panel.getChildren().add(molecule);
    }

    public static void managerShape(KeyEvent keyEvent) {
        switch (keyEvent.getCode()) {
            case  W:
                molecule.rotationByY(-10);
                break;
            case S:
                molecule.rotationByY(10);
                break;
            case A:
                molecule.rotationByX(-10);
                break;
            case D:
                molecule.rotationByX(10);
                break;
            case MINUS:
                changingTheSize(-100);
                break;
            case EQUALS:
                changingTheSize(+100);
                break;
        }
    }

    public static void changingTheSize(int par) {
        molecule.translateZProperty().set(molecule.getTranslateZ() + par);
    }

    public void save_picture(ActionEvent actionEvent) {
        Window stage = draw_panel.getScene().getWindow();
        fileChooser.setTitle("Save your picture");
        fileChooser.setInitialFileName("mypicture");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG file", "*.png"),
                new FileChooser.ExtensionFilter("JPG file", "*.jpg")
        );

        try {
            File file = fileChooser.showSaveDialog(stage);
            fileChooser.setInitialDirectory(file.getParentFile());

            File f = new File(file.getAbsolutePath());

            String fileExtension = f.getName().substring(f.getName().lastIndexOf(".") + 1);

            if(f != null) {
                Image image = draw_panel.snapshot(null, null);

                ImageIO.write(SwingFXUtils.fromFXImage(image,null),fileExtension,f);
            }
        } catch (Exception e) {
            System.out.println("lol");
        }
    }

    static class SmartGroup extends Group {
        public Rotate r;
        public  Transform t = new Rotate();

        public void rotationByX(int ang){
            Rotate r = new Rotate(ang,Rotate.X_AXIS);
            t = t.createConcatenation(r);
            molecule.getTransforms().clear();
            molecule.getTransforms().addAll(t);
        }

        public  void rotationByY(int ang){
            Rotate r = new Rotate(ang,Rotate.Y_AXIS);
            t = t.createConcatenation(r);
            molecule.getTransforms().clear();
            molecule.getTransforms().addAll(t);
        }

    }
}



package com.example.laba1task6;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.IOException;

public class MainApplication extends Application {

    public static final int HEIGHT = 600;
    public static final int WIDTH = 800;

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("main-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), WIDTH, HEIGHT);
        stage.setTitle("Task #6");
        stage.setScene(scene);
        stage.show();

        scene.setOnKeyPressed(MainController::managerShape);
    }

    public static void main(String[] args) {
        launch();
    }
}
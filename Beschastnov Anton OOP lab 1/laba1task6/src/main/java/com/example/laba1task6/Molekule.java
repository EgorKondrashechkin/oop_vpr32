package com.example.laba1task6;

import javafx.geometry.Point3D;
import javafx.scene.shape.Cylinder;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;

import java.util.ArrayList;

public class Molekule {

    private ArrayList<Atom> allAtom = new ArrayList<>();

    private String description;

    public ArrayList<Atom> getAllAtom() {
        return allAtom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setConnection(MainController.SmartGroup molecule) {
        for (var atom: allAtom) {
            for (var atom2: allAtom) {

                if (atom == atom2){
                    continue;
                }

                double X = Math.abs(atom.getX() - atom2.getX());
                double Y = Math.abs(atom.getY() - atom2.getY());
                double Z = Math.abs(atom.getZ() - atom2.getZ());

                if (X < 250 && Y < 250 && Z < 250) {
                    Cylinder cl = createConnection(
                            new Point3D(
                                    atom.getRepresentation().getParent().getTranslateX() +
                                            30,
                                    atom.getRepresentation().getParent().getTranslateY() +
                                            25,
                                    atom.getRepresentation().getParent().getTranslateZ() -
                                            5
                            ),
                            new Point3D(
                                    atom2.getRepresentation().getParent().getTranslateX() +
                                            30,
                                    atom2.getRepresentation().getParent().getTranslateY() +
                                            25,
                                    atom2.getRepresentation().getParent().getTranslateZ() -
                                            5
                            )
                    );

                    molecule.getChildren().add(cl);
                }
            }
        }
    }

    public Cylinder createConnection(Point3D origin, Point3D target) {
        Point3D yAxis = new Point3D(0, 1, 0); /* цилиндр изначально расположен вертикально
            (высота вдоль оси OY), направляющий вектор для оси OY - (0, 1, 0) */
        Point3D diff = target.subtract(origin); /* разность векторов target и origin - вектор,
                направленный от origin к target */
        double height = diff.magnitude(); // расстояние между origin и target - высота цилиндра
        Point3D mid = target.midpoint(origin); /* точка, лежащая посередине между target и
                origin - сюда нужно переместить цилиндр (поместить его центр) */
        Translate moveToMidpoint = new Translate(mid.getX(), mid.getY(), mid.getZ());
        Point3D axisOfRotation = diff.crossProduct(yAxis); /* ось, вокруг которой нужно
            повернуть цилиндр - направлена перпендикулярно плоскости, в которой лежат
            пересекающиеся вектора diff (направление от origin к target) и yAxis (текущее направление
            высоты цилиндра), получается как векторное произведение diff и yAxis */
        double angle = Math.acos(diff.normalize().dotProduct(yAxis)); /* угол поворота цилиндра -
                    угол между нормализованным (длина равна 1) вектором diff и вектором yAxis */
        Rotate rotateAroundCenter = new Rotate(-Math.toDegrees(angle), axisOfRotation);
        Cylinder line = new Cylinder(5, height); /* радиус цилиндра 1, нужно заменить на свое
        значение */
        line.getTransforms().addAll(moveToMidpoint, rotateAroundCenter);
        return line;
    }
}

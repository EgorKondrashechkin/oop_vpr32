package com.example.laba1task6;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;
import javafx.scene.text.Text;

public class Atom {

    private double x;

    private double y;

    private double z;

    private String type;

    private Sphere representation = new Sphere();

    public Atom(double x, double y, double z, String type) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.type = type;
    }

    public Atom() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    public Atom(String s) {
        String[] words =  s.split(" ");

        this.type = words[0];
        this.x = Double.parseDouble(words[1]);
        this.y = Double.parseDouble(words[2]);
        this.z = Double.parseDouble(words[3]);;


    }

    public void setAPositionSphere(double x, double y, double z) {
        representation.setTranslateX(x);
        representation.setTranslateY(y);
        representation.setTranslateZ(z);
    }

    public void setAPositionSphere() {
        representation.setTranslateX(this.x);
        representation.setTranslateY(this.y);
        representation.setTranslateZ(this.z);
        representation.setAccessibleText(type);
    }

    public void setAPositionStack(StackPane stack) {
        stack.setTranslateX(this.x);
        stack.setTranslateY(this.y);
        stack.setTranslateZ(this.z);
        stack.getChildren().addAll(representation, new Text(this.type));
    }

    public void setAtomParametr() {
        char schar[] = this.type.toCharArray();
        double code = 0;
        int radius = 0;

        for (var ch :schar) {
            radius += ch;
            code += (double) ch / 124;
        }

        if (code > 1) {
            System.out.println(code);
            code =  code/2;
            System.out.println(code);
        }

        representation.setRadius(raschetRadiasa(radius));

        PhongMaterial material = new PhongMaterial();

        Color curColor =  Color.color(Math.abs(1 - code),Math.abs(0.5 - code),Math.abs(0.3 - code));
        material.setDiffuseColor(curColor);
        representation.setMaterial(material);
    }

    private double raschetRadiasa(double radius) {

        radius = radius/2;
        radius = radius - 56;

        return Math.abs(radius);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Sphere getRepresentation() {
        return representation;
    }

    public void setRepresentation(Sphere representation) {
        this.representation = representation;
    }
}
